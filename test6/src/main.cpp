#include <concepts>
#include <iostream>
#include <memory>
#include <vector>

class Document {
public:
    virtual void process() = 0;
    virtual ~Document() = default;
};

class Markdown : public Document {
public:
    void process() { std::cout << "Markdown process" << std::endl; }
};
class HTML : public Document {
public:
    void process() { std::cout << "HTML process" << std::endl; }
};
int main() {
    Document *pDoc1 = new Markdown();
    std::unique_ptr<Document> pDoc2 = std::make_unique<Markdown>();
    std::shared_ptr<Document> pDoc3 = std::make_shared<Markdown>();
    delete pDoc1;
}