@startuml
title C++ Code Structure

package "Concepts" {
  class Document << (C,#FF7700) >> {
    +bool semiregular<T>()
    +void process()
  }
}

package "Classes" {
  class Markdown {
    +void process()
  }

  class HTML {
    +void process()
  }
}

package "Functions" {
  class invoke<T> {
    +invoke()
  }
}

package "Main Function" {
  class Main {
    +int main()
  }
}

Document <|.. Markdown
Document <|.. HTML

invoke -down-> Markdown : uses
invoke -down-> HTML : uses

Main --> invoke : invokes with Markdown

@enduml
