#include <concepts>
#include <iostream>
#include <vector>
template <typename T>
concept Document = std::semiregular<T> && requires(T t) { t.process(); };

class Markdown {
public:
    void process() { std::cout << "Markdown process" << std::endl; }
};
class HTML {
public:
    void process() { std::cout << "HTML process" << std::endl; }
};
template <Document T> void invoke() {
    T doc;
    doc.process();
    int count = 10;
    std::vector<T> vec(count);
    vec.push_back(Markdown());
    vec.push_back(Markdown());
    vec.push_back(Markdown());
    for (auto &elem : vec) {
        elem.process();
    }
}
int main() { invoke<Markdown>(); }