#include <gmock/gmock.h>
#include <gtest/gtest.h>

#pragma comment(lib, "gtest.lib")
#pragma comment(lib, "gmock_main.lib")

using namespace testing;

class Packet {};

class ConcretePacketStream {
public:
    void AppendPacket(Packet* new_packet) {}
    const Packet* GetPacket(size_t packet_number) const {
        std::cout << "GetPacket" << std::endl;
        return nullptr;
    }
    size_t NumberOfPackets() const { return 0; }
};

class MockPacketStream {
public:
    MOCK_CONST_METHOD1(GetPacket, const Packet*(size_t packet_number));
    MOCK_CONST_METHOD0(NumberOfPackets, size_t());
};

TEST(PacketStreamTest, SomeTest) {
    MockPacketStream mock_stream;

    // Expectation for GetPacket(0)
    EXPECT_CALL(mock_stream, GetPacket(0))
        .Times(1)                    // 期望被调用一次
        .WillOnce(Return(nullptr));  // 返回 nullptr

    // Expectation for NumberOfPackets()
    EXPECT_CALL(mock_stream, NumberOfPackets())
        .Times(1)              // 期望被调用一次
        .WillOnce(Return(0));  // 返回 0

    // 测试代码中未添加实际调用 Mock 对象方法的代码，这里可以根据具体逻辑添加

    // 模拟测试逻辑，例如：
    mock_stream.GetPacket(0);
    mock_stream.NumberOfPackets();
}

TEST(ConcretePacketStreamTest, AppendAndGetPacketTest) {
    ConcretePacketStream stream;
    Packet packet;

    // 测试 AppendPacket 方法
    stream.AppendPacket(&packet);

    // 测试 GetPacket 方法
    const Packet* result = stream.GetPacket(0);
    EXPECT_EQ(result, nullptr);  // 验证 GetPacket 返回值是否为 nullptr

    // 测试 NumberOfPackets 方法
    size_t numPackets = stream.NumberOfPackets();
    EXPECT_EQ(numPackets, 0);  // 验证 NumberOfPackets 返回值是否为 0
}
