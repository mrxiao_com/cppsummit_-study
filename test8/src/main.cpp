#include <iostream>
#include <string>
#include <vector>

template <typename T>
concept ListStrategy = requires(T t, const std::string &item) {
    t.list_item(item);
    t.start();
    t.end();
};
class MarkdownList {
public:
    void start() { std::cout << "MarkdownList start\n"; }
    void end() { std::cout << "MarkdownList end\n"; }
    void list_item(const std::string &item) {
        std::cout << "* " << item << "\n";
    }
};
class HtmlList {
public:
    void start() { std::cout << "<ul>\n"; }
    void end() { std::cout << "</ul>\n"; }
    void list_item(const std::string &item) {
        std::cout << "  <li>" << item << "</li>\n";
    }
};
template <ListStrategy T> class TextProcessor : private T {
public:
    void append_list(const std::vector<std::string> &items) {
        T::start();
        for (const auto &item : items)
            T::list_item(item);
        T::end();
    }
};
int main() {
    TextProcessor<MarkdownList> markdownProcessor;
    TextProcessor<HtmlList> htmlProcessor;
    std::vector<std::string> items = {"item1", "item2", "item3"};
    std::cout << "Markdown output:\n";
    markdownProcessor.append_list(items);
    std::cout << "\nHTML output:\n";
    htmlProcessor.append_list(items);
    return 0;
}
