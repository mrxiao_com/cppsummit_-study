class Vector {
public:
    Vector(int n = 0) : sz(n) {}
    ~Vector() {}

private:
    double *elem;
    int sz;
};

int main() {
    int n = 10;
    Vector v(n);
}