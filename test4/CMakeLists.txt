cmake_minimum_required(VERSION 3.5) # CMake install : https://cmake.org/download/
project(test4 LANGUAGES CXX)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_PREFIX_PATH "c:/Qt/6.7.0/msvc2019_64") # Qt Kit Dir
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
find_package(Qt6 REQUIRED COMPONENTS Core Gui Widgets OpenGLWidgets)

aux_source_directory(./src srcs)
include_directories(./src)
add_executable(${PROJECT_NAME}
    WIN32 # If you need a terminal for debug, please comment this statement 
    ${srcs}
) 

if (WIN32)
# Specify MSVC UTF-8 encoding   
add_compile_options("$<$<C_COMPILER_ID:MSVC>:/utf-8>")
add_compile_options("$<$<CXX_COMPILER_ID:MSVC>:/utf-8>")
endif()


target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Widgets Qt6::OpenGLWidgets Qt6::Gui Qt6::Core) # Qt5 Shared Library

if(CMAKE_HOST_UNIX) 
    set(CMAKE_INSTALL_RPATH "/usr/local/lib")
    set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    target_link_libraries(${PROJECT_NAME} PRIVATE opencv_stitching opencv_alphamat opencv_aruco opencv_barcode opencv_bgsegm opencv_bioinspired opencv_ccalib opencv_dnn_objdetect opencv_dnn_superres opencv_dpm opencv_face opencv_freetype opencv_fuzzy opencv_hdf opencv_hfs opencv_img_hash opencv_intensity_transform opencv_line_descriptor opencv_mcc opencv_quality opencv_rapid opencv_reg opencv_rgbd opencv_saliency opencv_shape opencv_stereo opencv_structured_light opencv_phase_unwrapping opencv_superres opencv_optflow opencv_surface_matching opencv_tracking opencv_highgui opencv_datasets opencv_text opencv_plot opencv_ml opencv_videostab opencv_videoio opencv_viz opencv_wechat_qrcode opencv_ximgproc opencv_video opencv_xobjdetect opencv_objdetect opencv_calib3d opencv_imgcodecs opencv_features2d opencv_dnn opencv_flann opencv_xphoto opencv_photo opencv_imgproc opencv_core)
    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
        "${CMAKE_CURRENT_SOURCE_DIR}/src/image.png"      
        $<TARGET_FILE_DIR:${PROJECT_NAME}>) 
elseif(CMAKE_HOST_WIN32)
endif()

if (WIN32)
    set(DEBUG_SUFFIX)
    if (MSVC AND CMAKE_BUILD_TYPE MATCHES "Debug")
        set(DEBUG_SUFFIX "d")
    endif ()
    set(QT_INSTALL_PATH "${CMAKE_PREFIX_PATH}")
    if (NOT EXISTS "${QT_INSTALL_PATH}/bin")
        set(QT_INSTALL_PATH "${QT_INSTALL_PATH}/..")
        if (NOT EXISTS "${QT_INSTALL_PATH}/bin")
            set(QT_INSTALL_PATH "${QT_INSTALL_PATH}/..")
        endif ()
    endif ()
    if (EXISTS "${QT_INSTALL_PATH}/plugins/platforms/qwindows${DEBUG_SUFFIX}.dll")
        add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E make_directory
                "$<TARGET_FILE_DIR:${PROJECT_NAME}>/plugins/platforms/")
        add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E copy
                "${QT_INSTALL_PATH}/plugins/platforms/qwindows${DEBUG_SUFFIX}.dll"
                "$<TARGET_FILE_DIR:${PROJECT_NAME}>/plugins/platforms/")
    endif ()
    foreach (QT_LIB Core Gui Widgets Multimedia Network OpenGLWidgets OpenGL)
        add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E copy
                "${QT_INSTALL_PATH}/bin/Qt6${QT_LIB}${DEBUG_SUFFIX}.dll"
                "$<TARGET_FILE_DIR:${PROJECT_NAME}>")
    endforeach (QT_LIB)

    # add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
    # COMMAND ${CMAKE_COMMAND} -E copy_if_different
    # "${CMAKE_CURRENT_SOURCE_DIR}/../windowslibrary/ffmpeg/bin/avcodec-61.dll"
    # "${CMAKE_CURRENT_SOURCE_DIR}/../windowslibrary/ffmpeg/bin/avutil-59.dll"  
    # "${CMAKE_CURRENT_SOURCE_DIR}/../windowslibrary/ffmpeg/bin/libx265.dll"  
    # "${CMAKE_CURRENT_SOURCE_DIR}/../windowslibrary/ffmpeg/bin/libx264-164.dll"
    # "${CMAKE_CURRENT_SOURCE_DIR}/../windowslibrary/ffmpeg/bin/fdk-aac.dll"     
    # "${CMAKE_CURRENT_SOURCE_DIR}/../windowslibrary/ffmpeg/bin/swresample-5.dll" 
    # "${CMAKE_CURRENT_SOURCE_DIR}/../windowslibrary/ffmpeg/bin/avformat-61.dll" 
    # "${CMAKE_CURRENT_SOURCE_DIR}/../windowslibrary/SDL/bin/SDL3.dll"   
       
    #     $<TARGET_FILE_DIR:${PROJECT_NAME}>) 

endif ()


# 对于Windows平台，确保可执行文件有控制台
if(WIN32)
    set_target_properties(${PROJECT_NAME}  PROPERTIES WIN32_EXECUTABLE false)
endif()