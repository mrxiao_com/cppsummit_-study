#include <concepts>
#include <iostream>
template <typename T>
concept Document = std::semiregular<T> && requires(T t) { t.process(); };
template <Document T> void invoke() {
    T doc;
    doc.process();
}
class Markdown {
public:
    void process() { std::cout << "Markdown process" << std::endl; }
};
class HTML {
public:
    void process() { std::cout << "HTML process" << std::endl; }
};
int main() { invoke<Markdown>(); }