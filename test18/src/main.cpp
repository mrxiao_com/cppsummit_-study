#include <gtest/gtest.h>

#include <tuple>

using namespace testing;
class Rectangle {
public:
    Rectangle(int length, int width) : length_(length), width_(width) {}
    int perimeter() const { return 2 * (length_ + width_); }

private:
    int length_;
    int width_;
};
// RectangleTest 测试类
class RectangleTest
    : public ::testing::TestWithParam<std::tuple<int, int, int>> {};
// 周长测试
TEST_P(RectangleTest, perimeter_of_rectangle_should_be_2_X_length_width) {
    auto test_data = GetParam();
    Rectangle rectangle(std::get<0>(test_data), std::get<1>(test_data));
    ASSERT_EQ(rectangle.perimeter(), std::get<2>(test_data));
}
// 实例化测试数据
INSTANTIATE_TEST_SUITE_P(perimeter, RectangleTest,
                         ::testing::Values(std::make_tuple(5, 8, 26),
                                           std::make_tuple(1, 4, 10),
                                           std::make_tuple(10, 30, 80)));
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
