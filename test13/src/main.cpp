#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <iostream>

using namespace testing;

// 假设 Packet 类已经定义
class Packet {};

// MockPacketStream 类的定义
class MockPacketStream {
public:
    MOCK_METHOD(void, AppendPacket, (Packet * new_packet));
    MOCK_CONST_METHOD1(GetPacket, const Packet*(size_t packet_number));
    MOCK_CONST_METHOD0(NumberOfPackets, size_t());
};

// PacketReader 模板类的定义
template <class PacketStream>
class PacketReader {
public:
    void ReadPackets(PacketStream* stream, size_t packet_num) {
        for (size_t i = 0; i < packet_num; ++i) {
            const Packet* packet = stream->GetPacket(i);
            // 处理 packet 的逻辑，这里简化为输出信息
            std::cout << "Packet " << i << ": " << (packet ? "Valid" : "Null")
                      << std::endl;
        }
    }
};

// 定义单元测试
TEST(PacketReaderTest, ReadPacketsTest) {
    MockPacketStream mock_stream;
    PacketReader<MockPacketStream> reader;

    // 设置期望：MockPacketStream 的行为期望
    // 这个代码是设置期望值
    EXPECT_CALL(mock_stream, GetPacket(0)).Times(1).WillOnce(Return(nullptr));

    EXPECT_CALL(mock_stream, GetPacket(1))
        .Times(1)
        .WillOnce(Return(new Packet()));  // 假设返回一个有效的 Packet 对象

    EXPECT_CALL(mock_stream, NumberOfPackets())
        .Times(AnyNumber())  // Times(AnyNumber()) 表示调用次数不限
        .WillRepeatedly(Return(2));

    // 执行测试逻辑：使用 PacketReader 读取数据流
    reader.ReadPackets(&mock_stream, 2);  // 读取两个数据包
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
