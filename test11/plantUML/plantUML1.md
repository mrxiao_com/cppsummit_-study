@startuml
title C++ 代码结构

package "概念" {
  class ListStrategy << (C,#FF7700) >> {
    +list_item(const std::string &item)
    +start()
    +end()
  }
}

package "类" {
  class MarkdownList {
    +start()
    +end()
    +list_item(const std::string &item)
  }

  class HtmlList {
    +start()
    +end()
    +list_item(const std::string &item)
  }

  class TextProcessor<T> {
    +append_list(const std::vector<std::string> &items)
  }
}

ListStrategy <|.. MarkdownList : 实现
ListStrategy <|.. HtmlList : 实现

TextProcessor o-- ListStrategy : 使用概念
TextProcessor *-- "T" : 组合

class Main {
    +int main()
}

Main --> TextProcessor : 使用

@enduml
