#include <string>
template <class T> class Vector {
public:
    Vector(int n = 0) : sz(n) {}
    ~Vector() {}

private:
    T *elem;
    int sz;
};
class File_handle {};

int main() {
    int n = 10;
    Vector<int> v(n);
    Vector<std::string> vs;
    Vector<File_handle> f(3);
}