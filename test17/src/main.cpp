#include <condition_variable>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

// 缓冲区的最大大小
const size_t BUFFER_SIZE = 10;

// 共享缓冲区
std::queue<int> buffer;
std::mutex buffer_mutex;
std::condition_variable buffer_not_full;
std::condition_variable buffer_not_empty;

// 生产者函数
void producer(int id) {
    int value = 0;
    while (true) {
        // 模拟生产数据
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        std::unique_lock<std::mutex> lock(buffer_mutex);

        // 等待缓冲区不满
        buffer_not_full.wait(lock, [] { return buffer.size() < BUFFER_SIZE; });

        // 生产数据并放入缓冲区
        buffer.push(value);
        std::cout << "Producer " << id << " produced " << value++ << std::endl;

        // 通知消费者缓冲区不空
        buffer_not_empty.notify_all();
    }
}

// 消费者函数
void consumer(int id) {
    while (true) {
        std::unique_lock<std::mutex> lock(buffer_mutex);

        // 等待缓冲区不空
        buffer_not_empty.wait(lock, [] { return !buffer.empty(); });

        // 从缓冲区取出数据
        int value = buffer.front();
        buffer.pop();
        std::cout << "Consumer " << id << " consumed " << value << std::endl;

        // 通知生产者缓冲区不满
        buffer_not_full.notify_all();

        // 模拟处理数据
        std::this_thread::sleep_for(std::chrono::milliseconds(150));
    }
}

int main() {
    // 创建生产者和消费者线程
    std::vector<std::thread> producers;
    std::vector<std::thread> consumers;

    for (int i = 0; i < 3; ++i) {
        producers.emplace_back(producer, i);
    }

    for (int i = 0; i < 3; ++i) {
        consumers.emplace_back(consumer, i);
    }

    // 等待所有线程完成
    for (auto& p : producers) {
        p.join();
    }

    for (auto& c : consumers) {
        c.join();
    }

    return 0;
}
