@startuml
title 模板方法模式类图

abstract class AbstractClass {
    TemplateMethod()
    PrimitiveOperation1()
    PrimitiveOperation2()
}

class ConcreteClass {
    PrimitiveOperation1()
    PrimitiveOperation2()
}

AbstractClass <|-- ConcreteClass

note right of AbstractClass
  TemplateMethod()
  PrimitiveOperation1()
  PrimitiveOperation2()
end note

note right of ConcreteClass
  PrimitiveOperation1()
  PrimitiveOperation2()
end note

@enduml
