#include <iostream>
#include <memory>

class Document {
public:
    virtual void process() = 0;
    virtual ~Document() = default;
};

class MarkDown : public Document {
public:
    void process() override { std::cout << "Markdown process" << std::endl; }
};
class HTML : public Document {
public:
    void process() override { std::cout << "HTML process" << std::endl; }
};
int main() {
    auto pDoc = std::make_unique<MarkDown>();
    pDoc->process();
}