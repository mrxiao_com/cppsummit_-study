#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <vector>

using namespace testing;

// Notify 类的定义
class Notify {
public:
    virtual ~Notify() = default;
    virtual void Update(int msg) = 0;
};

// MockNotify 类的定义，用于测试
class MockNotify : public Notify {
public:
    MOCK_METHOD(void, Update, (int msg), (override));
};

// Publisher 类的定义
class Publisher {
public:
    static Publisher& Instance() {
        static Publisher instance;
        return instance;
    }
    void Register(const std::shared_ptr<Notify>& notify) {
        notify_list_.push_back(notify);
    }
    void Broadcast(int msg) {
        for (const auto& notify : notify_list_) {
            notify->Update(msg);
        }
    }
    void Clear() { notify_list_.clear(); }

private:
    Publisher() = default;
    std::vector<std::shared_ptr<Notify>> notify_list_;
};

// PublisherTest 测试组
TEST(PublisherTest, empty_publisher_should_broadcast_nothing) {
    Publisher& publisher = Publisher::Instance();

    // 模拟一个消息广播
    publisher.Broadcast(42);

    // 没有通知者注册，因此没有期望设置
}

TEST(PublisherTest,
     publisher_with_1_subscriber_should_send_notification_to_this_subscriber) {
    Publisher& publisher = Publisher::Instance();

    // 创建一个 MockNotify 实例
    auto mock_notify = std::make_shared<MockNotify>();

    // 设置期望：mock_notify 应该收到一个消息，值为 42
    EXPECT_CALL(*mock_notify, Update(42)).Times(1);

    // 注册通知者
    publisher.Register(mock_notify);

    // 广播消息
    publisher.Broadcast(42);
}

TEST(PublisherTest,
     publisher_with_2_subscribers_should_broadcast_notifications) {
    Publisher& publisher = Publisher::Instance();
    publisher.Clear();
    // 创建两个 MockNotify 实例
    auto mock_notify1 = std::make_shared<MockNotify>();
    auto mock_notify2 = std::make_shared<MockNotify>();

    // 设置期望：两个 mock_notify 应该收到相同的消息，值为 42
    EXPECT_CALL(*mock_notify1, Update(42)).Times(1);
    EXPECT_CALL(*mock_notify2, Update(42)).Times(1);

    // 注册通知者
    publisher.Register(mock_notify1);
    publisher.Register(mock_notify2);

    // 广播消息
    publisher.Broadcast(42);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
