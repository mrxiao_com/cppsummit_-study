class Engine {
public:
    void Start() {}
};
class Tyre {
public:
    void Inflate() {}
};
struct Car {
    Car(Engine& engine, Tyre& tyre) : engine(engine), tyre(tyre) {}
    bool Run() {
        engine.Start();
        return true;  // 假设启动引擎总是成功
    }
    Tyre& GetTyre() { return tyre; }

private:
    Engine& engine;
    Tyre& tyre;
};
int main() {
    Engine engine;
    Tyre tyre;
    Car car(engine, tyre);
    // 运行汽车
    bool isRunning = car.Run();
    // 使用auto&来接收GetTyre的返回值
    auto& retTyre = car.GetTyre();
    retTyre.Inflate();
    return 0;
}
