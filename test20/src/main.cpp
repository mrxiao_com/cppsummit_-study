class Engine;
class Tyre;
struct Car {
    Car(Engine* engine, Tyre* tyre) : engine(engine), tyre(tyre) {}
    bool Run() {
        if (engine == nullptr) {
            return false;
        }
        return true;
    }
    Tyre* GetTyre() { return tyre; }

private:
    Engine* engine;
    Tyre* tyre;
};
