@startuml
participant Producer1 as P1
participant Producer2 as P2
participant Producer3 as P3
participant Buffer
participant Consumer1 as C1
participant Consumer2 as C2
participant Consumer3 as C3

P1 -> Buffer: Wait until buffer is not full
P1 -> Buffer: Add item to buffer
Buffer -> P1: Notify buffer is not empty

P2 -> Buffer: Wait until buffer is not full
P2 -> Buffer: Add item to buffer
Buffer -> P2: Notify buffer is not empty

P3 -> Buffer: Wait until buffer is not full
P3 -> Buffer: Add item to buffer
Buffer -> P3: Notify buffer is not empty

C1 -> Buffer: Wait until buffer is not empty
C1 -> Buffer: Remove item from buffer
Buffer -> C1: Notify buffer is not full

C2 -> Buffer: Wait until buffer is not empty
C2 -> Buffer: Remove item from buffer
Buffer -> C2: Notify buffer is not full

C3 -> Buffer: Wait until buffer is not empty
C3 -> Buffer: Remove item from buffer
Buffer -> C3: Notify buffer is not full
@enduml
