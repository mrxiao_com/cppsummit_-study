#include <iostream>

class Library {
public:
    void process() {
        step1();
        while (!step2())
            step3();
        step4();
        step5();
    }
    virtual ~Library() {}

protected:
    void step3() { std::cout << "Library step3\n"; }
    void step1() { std::cout << "Library step1\n"; }
    virtual bool step2() = 0;
    virtual int step4() = 0;
    virtual void step5() = 0;
};

class App : public Library {
protected:
    bool step2() override {
        std::cout << "App step2\n";
        return true; // 为了使循环结束
    }
    int step4() override {
        std::cout << "App step4\n";
        return 0;
    }
    void step5() override { std::cout << "App step5\n"; }
};

int main() {
    App myApp;
    myApp.process();
    return 0;
}
