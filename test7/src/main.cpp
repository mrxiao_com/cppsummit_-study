#include <concepts>

template <typename T>
concept TxtDocument = std::semiregular<T> && requires(T t) {
    t.process();
    t.extend();
    t.process_imp();
    t.read_text();
};