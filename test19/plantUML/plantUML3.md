@startuml test1
participant Observer
participant Driver
activate Observer #Blue
Observer -> Observer:Designing...
Observer -> Observer:Test...
Observer -> Driver:EV_TEST_COMPLETED_IND
activate Driver #Blue
Driver -> Driver:Coding..
Driver -> Driver:Refactoring...
Observer <-- Driver:EV_REFACTOR_COMPLETED_IND
@enduml

@startuml test2
participant Copilot 
participant Driver
activate Copilot #Blue
Copilot -> Copilot:Route Planning...
Copilot -> Driver:EV_ROUTE_PLAN_COMPLETED_IND
activate Driver #Blue
Driver -> Driver:Driving...
Copilot <- Driver:EV_DRIVING_START_IND
Copilot -> Copilot:Navigating...

Copilot --> Driver:EV_REACH_THE_GOAL_IND
Driver -> Driver:Parking...
@enduml