#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <functional>
#include <tuple>

// 假设 Orientation 类已经定义
class Orientation {
public:
    enum Direction { NC, E, SC, W };
    Orientation(Direction dir) : dir_(dir) {}
    Orientation turnLeft() const {
        switch (dir_) {
            case NC:
                return Orientation(W);
            case E:
                return Orientation(NC);
            case SC:
                return Orientation(E);
            case W:
                return Orientation(SC);
        }
        // Default case to handle any unexpected value
        return *this;
    }
    Orientation turnRight() const {
        switch (dir_) {
            case NC:
                return Orientation(E);
            case E:
                return Orientation(SC);
            case SC:
                return Orientation(W);
            case W:
                return Orientation(NC);
        }
        // Default case to handle any unexpected value
        return *this;
    }
    bool operator==(const Orientation& other) const {
        return dir_ == other.dir_;
    }

private:
    Direction dir_;
};

// OrientationTurnTest 测试类
class OrientationTurnTest
    : public ::testing::TestWithParam<std::tuple<
          Orientation, std::function<Orientation(Orientation&)>, Orientation>> {
};
TEST_P(OrientationTurnTest, should_turn_correctly) {
    auto [initial, turnFunction, expected] = GetParam();
    Orientation result = turnFunction(initial);
    ASSERT_EQ(result, expected);
}
Orientation TurnLeft(Orientation& origin) { return origin.turnLeft(); }
Orientation TurnRight(Orientation& origin) { return origin.turnRight(); }
INSTANTIATE_TEST_SUITE_P(
    left_right_data, OrientationTurnTest,
    ::testing::Values(std::make_tuple(Orientation(Orientation::NC), TurnRight,
                                      Orientation(Orientation::E)),
                      std::make_tuple(Orientation(Orientation::E), TurnLeft,
                                      Orientation(Orientation::NC)),
                      std::make_tuple(Orientation(Orientation::E), TurnRight,
                                      Orientation(Orientation::SC)),
                      std::make_tuple(Orientation(Orientation::SC), TurnLeft,
                                      Orientation(Orientation::E))));
int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
