#include <functional>
#include <iostream>
#include <list>

// 标准库定义概念（已包含在 C++20 中）
template <class F, class... Args>
concept invocable = requires(F &&f, Args &&...args) {
    std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
};

struct ProgressObserver {
    void operator()(float value) {
        // ... 实现细节
        std::cout << "ProgressObserver called with " << value << std::endl;
    }
};

// 别名模板不是必需的，但在这里保持与您的原始代码一致
using ProgressHandler = std::function<void(float)>;

// 假设我们有一个 lambda 表达式作为回调
auto callback = [](float value) {
    // ... 实现细节
    std::cout << "Lambda callback called with " << value << std::endl;
};

// Subject 类现在接受一个满足 invocable<float> 的类型作为模板参数
template <typename ProgressObsT>
    requires std::invocable<ProgressObsT, float>
class Subject {
private:
    std::list<ProgressObsT>
        m_progressList;  // 使用 std::list 和正确的成员变量名

protected:
    void onUpdate(float value) {
        for (auto &progress : m_progressList) {
            progress(value);  // 调用 ProgressObsT 的 operator()
        }
    }

public:
    void addProgress(const ProgressObsT &progress) {
        m_progressList.push_back(progress);  // 添加观察者
    }

    // 使用 std::remove_if 和 std::list::erase 来移除元素
    void removeProgress(const ProgressObsT &progress) {
        m_progressList.erase(
            std::remove_if(
                m_progressList.begin(), m_progressList.end(),
                [&progress](const ProgressObsT &p) { return &p == &progress; }),
            m_progressList.end());
    }

    // 假设有一个公共方法来触发更新
    void triggerUpdate(float value) { onUpdate(value); }
};

int main() {
    Subject<ProgressObserver> subjectObserver;
    subjectObserver.addProgress(
        ProgressObserver());              // 添加 ProgressObserver 实例
    subjectObserver.triggerUpdate(0.5f);  // 触发更新并调用 ProgressObserver

    Subject<ProgressHandler> subjectLambda;
    subjectLambda.addProgress(callback);  // 添加 lambda 表达式
    subjectLambda.triggerUpdate(0.75f);  // 触发更新并调用 lambda 表达式

    return 0;
}