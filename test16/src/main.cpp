#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <condition_variable>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

using namespace testing;

class SyncPoint {
public:
    SyncPoint(const std::string& point) : point_(point) {}

    SyncPoint& With(const std::string& point,
                    const std::thread::id& thread_id) {
        std::unique_lock<std::mutex> lock(mutex_);
        sync_points_[point].push_back(thread_id);
        if (sync_points_[point].size() == 2) {
            cv_.notify_all();
        }
        cv_.wait(lock, [&] { return sync_points_[point].size() >= 2; });
        return *this;
    }

private:
    std::string point_;
    std::mutex mutex_;
    std::condition_variable cv_;
    static std::unordered_map<std::string, std::vector<std::thread::id>>
        sync_points_;
};

std::unordered_map<std::string, std::vector<std::thread::id>>
    SyncPoint::sync_points_;

class ThreadController {
public:
    SyncPoint& Sync(const std::string& point,
                    const std::thread::id& thread_id) {
        return SyncPoint(point).With(point, thread_id);
    }
};

ThreadController threadController;

void inject_point(const std::string& point) {
    threadController.Sync(point, std::this_thread::get_id());
}

void foo() {
    inject_point("foo first step complete");
    std::cout << "foo finished" << std::endl;
}

void bar() {
    inject_point("foo first step complete");
    std::cout << "bar finished" << std::endl;
}

TEST(multi_thread_test, 2_thread_synchronize) {
    auto foo_thr = std::thread(foo);
    auto bar_thr = std::thread(bar);
    threadController.Sync("foo first step complete", foo_thr.get_id())
        .With("foo first step complete", bar_thr.get_id());
    foo_thr.join();
    bar_thr.join();
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
